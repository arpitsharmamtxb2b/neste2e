export interface LoginRequestDto {
    readonly email: string;
    readonly password: string;
}

export interface LoginResponseDto {
    readonly jwt: string;
}

export interface NewUserRequestDto {
    readonly email: string;
    readonly password: string;
    readonly name: string;
}

export const DefaultCredentials: LoginRequestDto = {
    email: 'test@gmail.com',
    password: '123'
}