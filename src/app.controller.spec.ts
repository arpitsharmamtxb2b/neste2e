import { HttpException, HttpStatus, INestApplication } from '@nestjs/common';
import { DefaultCredentials, LoginRequestDto, LoginResponseDto } from './dto/e2e-test.dto';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ErrorMessage } from './constants/errors';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from './app.module';
import * as request from 'supertest';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from './schemas/user.schema';
import { closeInMongodConnection, rootMongooseTestModule } from './mongo-test-module/mongo-test-module.module';

describe('AppController', () => {
  let appController: AppController;
  let appService: AppService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        rootMongooseTestModule(),
        MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
      ],
      controllers: [AppController],
      providers: [AppService],
    }).compile();

    appController = module.get<AppController>(AppController);
    appService = module.get<AppService>(AppService);
  });

  describe('Unit Test Cases ', () => {

    it('should add new user', async () => {
      const newUser = {
        name: 'Arpit Sharma',
        email: 'test@gmail.com',
        password: '123'
      }
      expect(await appService.createUser(newUser));
    })

    it('should not add new user', async () => {
      await appService.createUser({
        name: 'Arpit Sharma',
        email: 'test@gmail.com',
        password: '123'
      });
      const newUser = {
        name: 'Arpit Sharma',
        email: 'test@gmail.com',
        password: '123'
      }
      try {
        await appService.createUser(newUser)
      } catch (error) {
        expect(error.response).toEqual(ErrorMessage.emailAlreadyExists);
        expect(error.status).toEqual(HttpStatus.CONFLICT);
      };
    })

    it('should provide jwt token on signin', async () => {
      const newUser = {
        name: 'Arpit Sharma',
        email: 'test@gmail.com',
        password: '123'
      }
      expect(await appService.createUser(newUser));
      expect(await (await appController.login({
        email: "test@gmail.com",
        password: "123"
      })).jwt).toBe('token');
    });

    it('should throw error', async () => {
      const cred: LoginRequestDto = {
        email: 'ok@gmail.com',
        password: '123'
      };
      try {
        await appService.login(cred);
      } catch (error) {
        expect(error.response).toEqual(ErrorMessage.invalidCredentials);
        expect(error.status).toEqual(HttpStatus.NOT_FOUND);
      }
    })

    it('should get list of users', async () => {
      const newUser = {
        name: 'Arpit Sharma',
        email: 'test@gmail.com',
        password: '123'
      }
      expect(await appService.createUser(newUser));
      expect((await appService.getUsers()).length).toBeGreaterThan(0)
    });

  });

});

describe('E2E test cases', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        rootMongooseTestModule(),
        MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
      ],
      controllers: [AppController],
      providers: [AppService],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/new-user - add new user - (POST)', () => {
    return request(app.getHttpServer())
      .post('/new-user')
      .send({
        name: 'Arpit Sharma',
        email: 'lazycoderr.arpit@gmail.com',
        password: '123'
      }).then(data => {
        expect(data.status).toBe(200);
      })
  });

  it('/new-user - add existing user - (POST)', () => {
    return request(app.getHttpServer())
      .post('/new-user')
      .send({
        name: 'Arpit Sharma',
        email: 'lazycoderr.arpit@gmail.com',
        password: '123'
      }).then(data => request(app.getHttpServer())
        .post('/new-user')
        .send({
          name: 'Arpit Sharma',
          email: 'lazycoderr.arpit@gmail.com',
          password: '123'
        }).then(data => {
          expect(data.body.statusCode).toBe(HttpStatus.CONFLICT);
          expect(data.body.message).toBe(ErrorMessage.emailAlreadyExists);
        }));
  });

  it('/login - right credentials -  (POST)', () => {
    return request(app.getHttpServer())
      .post('/new-user')
      .send({
        name: 'Arpit Sharma',
        email: 'lazycoderr.arpit@gmail.com',
        password: '123'
      }).then(data => request(app.getHttpServer())
        .post('/login')
        .send({
          email: 'lazycoderr.arpit@gmail.com',
          password: '123'
        })
        .then(data => {
          expect(data.body.jwt).toBe('token')
        }));
  });

  it('/login - wrong credentials -  (POST)', () => {
    return request(app.getHttpServer())
      .post('/login')
      .send({
        email: 'arpitsharma@mtxb2b.com',
        password: '123'
      })
      .then(data => {
        expect(data.body.message).toBe(ErrorMessage.invalidCredentials);
        expect(data.body.statusCode).toBe(HttpStatus.NOT_FOUND);
      })
  });

  it('/ - Get List - (GET)', () => {
    return request(app.getHttpServer())
      .post('/new-user')
      .send({
        name: 'Arpit Sharma',
        email: 'lazycoderr.arpit@gmail.com',
        password: '123'
      }).then(data => request(app.getHttpServer())
        .get('/')
        .then(data => {
          expect(data.body.length).toBeGreaterThan(0);
        }));
  })

  afterAll(async () => {
    await app.close();
  });

});
