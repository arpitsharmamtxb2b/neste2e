import { HttpCode, HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ErrorMessage } from './constants/errors';
import { Users } from './data/dummy';
import { DefaultCredentials, LoginRequestDto, LoginResponseDto, NewUserRequestDto } from './dto/e2e-test.dto';
import { User, UserDocument } from './schemas/user.schema';

@Injectable()
export class AppService {

  users = Users;

  constructor(
    @InjectModel(User.name) private readonly _userModel: Model<UserDocument>
  ) { }

  getHello(): string {
    return 'Hello World!';
  }

  async login(loginRequestDto: LoginRequestDto): Promise<LoginResponseDto> {
    const user = await this._userModel.findOne({
      $and: [
        { email: loginRequestDto.email },
        { password: loginRequestDto.password }
      ]
    }).exec()
    if (user) {
      return {
        jwt: 'token'
      }
    }
    throw new HttpException(ErrorMessage.invalidCredentials, HttpStatus.NOT_FOUND);
  }

  async createUser(newUserRequestDto: NewUserRequestDto) {
    const users = await this._userModel.find({ email: newUserRequestDto.email }).exec()
    if (users.length > 0) {
      throw new HttpException(ErrorMessage.emailAlreadyExists, HttpStatus.CONFLICT);
    }
    const user = new this._userModel(newUserRequestDto);
    await user.save();
  }

  async getUsers(): Promise<any> {
    return this._userModel.find().exec();
  }

}
