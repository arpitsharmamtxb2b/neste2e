export const ErrorMessage = Object.freeze({
    invalidCredentials: 'Invalid Credentials',
    emailAlreadyExists: 'Email already exists'
})