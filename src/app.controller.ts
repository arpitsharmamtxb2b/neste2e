import { Body, Controller, Get, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { AppService } from './app.service';
import { LoginRequestDto, LoginResponseDto, NewUserRequestDto } from './dto/e2e-test.dto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  async getUsers(): Promise<any> {
    return await this.appService.getUsers();
  }

  @Post('login')
  async login(@Body() loginRequestDto: LoginRequestDto): Promise<LoginResponseDto> {
    return await this.appService.login(loginRequestDto);
  }

  @Post('/new-user')
  @HttpCode(200)
  async createNewUser(@Body() newUserRequestDto: NewUserRequestDto){
    return await this.appService.createUser(newUserRequestDto);
  }

}
