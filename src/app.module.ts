import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Mongoose } from 'mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { User, UserSchema } from './schemas/user.schema';


// connection string: mongodb+srv://mac:Arpit123@cluster0.gobcf.mongodb.net/test

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost:27017/e2e'),
    MongooseModule.forFeature([
      { name: User.name, schema: UserSchema }
    ])
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
